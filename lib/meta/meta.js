const getMeta = ({ description }) => ({
  type: 'layout',
  docs: {
    category: 'layout & Formatting',
    description,
    recommended: false,
    url: 'https://gitlab.com/gabortill-admin/eslint-plugin-consistent-file-and-folder-names/-/blob/master/Readme.md',
  },
  schema: [
    {
      additionalProperties: {
        type: 'string',
      },
    },
  ],
})

module.exports = getMeta
