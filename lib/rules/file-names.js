const path = require('path')
const process = require('process')
const getMeta = require('../meta/meta')
const namingConvention = require('../constants/naming-convention')

module.exports = {
  meta: getMeta({
    description: 'Enforce consistent file naming convention',
  }),
  create(context) {
    return {
      Program(node) {
        const rules = context.options[0]

        for (const [folderPath, desiredNamingConvention] of Object.entries(
          rules
        )) {
          const filename = context.getFilename()
          const splittedFolderPath = folderPath.split('/')
          const deepestFolderName =
            splittedFolderPath[splittedFolderPath.length - 1]
          const separator = process.platform === 'win32' ? '\\' : '/'
          const deepestFolderNameIndex = filename
            .split(separator)
            .findIndex((item) => item === deepestFolderName)
          const isDeepestFolderNameExistInCurrentPath =
            deepestFolderNameIndex > -1

          if (isDeepestFolderNameExistInCurrentPath) {
            const isPredefinedNaming = Object.keys(namingConvention).includes(
              desiredNamingConvention
            )
            const expression = isPredefinedNaming
              ? namingConvention[desiredNamingConvention]
              : desiredNamingConvention
            const isMatch = new RegExp(expression).test(
              path.parse(filename).name
            )

            if (!isMatch) {
              context.report(
                node,
                `Filname "${path.basename(
                  filename
                )}" in path "${filename}" does not match with the "${desiredNamingConvention}" naming convention`
              )
            }
          }
        }
      },
    }
  },
}
