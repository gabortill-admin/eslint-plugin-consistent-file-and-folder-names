const getMeta = require('../meta/meta')
const namingConvention = require('../constants/naming-convention')

module.exports = {
  meta: getMeta({
    description: 'Enforce consistent folder naming',
  }),
  create(context) {
    return {
      Program(node) {
        const rules = context.options[0]
        const separator = process.platform === 'win32' ? '\\' : '/'

        for (const [folderPath, desiredNamingConvention] of Object.entries(
          rules
        )) {
          const filename = context.getFilename()
          const splitedFilePath = filename.split(separator)
          const rootFolderIndex = splitedFilePath.findIndex(
            (item) => item === folderPath
          )
          const isRootFolderExist = rootFolderIndex > -1

          if (isRootFolderExist) {
            const selection = splitedFilePath.slice(rootFolderIndex + 1, -1)

            selection.forEach((directory) => {
              const isPredefinedNaming = Object.keys(namingConvention).includes(
                desiredNamingConvention
              )
              const expression = isPredefinedNaming
                ? namingConvention[desiredNamingConvention]
                : desiredNamingConvention
              const isMatch = new RegExp(expression).test(directory)

              if (!isMatch) {
                context.report(
                  node,
                  `Foldername "${directory}" in path "${filename}" does not match with the "${desiredNamingConvention}" naming convention`
                )
              }
            })
          } else {
            context.report(
              node,
              `The provided folder "${folderPath}" does not exist`
            )
          }
        }
      },
    }
  },
}
