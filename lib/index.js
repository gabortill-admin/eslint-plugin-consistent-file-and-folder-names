module.exports = {
  rules: {
    'file-names': require('./rules/file-names'),
    'folder-names': require('./rules/folder-names'),
  },
}
