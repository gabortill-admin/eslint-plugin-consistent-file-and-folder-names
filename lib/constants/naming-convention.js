const namingConvention = {
  FLAT_CASE: /^[a-z0-9]+$/,
  CAMEL_CASE: /^([a-z]+){1}([A-Z][a-z0-9]*)*$/,
  PASCAL_CASE: /^([A-Z][a-z0-9]*)*$/,
  SNAKE_CASE: /^([a-z]+_?)([a-z0-9]+_)*[a-z0-9]+$/,
  KEBAB_CASE: /^([a-z]+-?)([a-z0-9]+-)*[a-z0-9]+$/,
  SCREAMING_SNAKE_CASE: /^([A-Z]+_?)([A-Z0-9]+_)*[A-Z0-9]+$/,
}

module.exports = namingConvention
