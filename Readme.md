# eslint-plugin-consistent-file-and-folder-names

ESLint plugin to enforce consistent file and folder names

## Installation

Make sure you have ESLint installed

```
npm i -D eslint
```

Next, you can install `eslint-plugin-consistent-file-and-folder-names`

```
npm i -D eslint-plugin-consistent-file-and-folder-names
```

## Usage

Add `eslint-plugin-consistent-file-and-folder-names` to the `plugins` section of your ESLint config file without the `eslint-plugin-` prefix

```json
{
  "plugins": ["consistent-file-and-folder-names"]
}
```

Then you have to extend the `rules` section to configure the this plugin based on your requirements

```json
{
  "plugins": ["consistent-file-and-folder-names"],
  "rules": {
    "consistent-file-and-folder-names/file-names": [
      "error",
      {
        "path-to-folder": "CAMEL_CASE",
        "path-to-another-folder/nested-folder": "^[a-zA-Z0-9]+$"
      }
    ],
    "consistent-file-and-folder-names/folder-names": [
      "error",
      {
        "path_to_folder": "SNAKE_CASE",
        "path-to-another-folder/nested-folder": "KEBAB_CASE"
      }
    ]
  }
}
```

## Supported naming conventions

You can set up one of the following naming conventions or any valid regular expression

| Name                 | Example     |
| -------------------- | ----------- |
| CAMEL_CASE           | helloWorld  |
| PASCAL_CASE          | HelloWorld  |
| SNAKE_CASE           | hello_world |
| KEBAB_CASE           | hello-world |
| SCREAMING_SNAKE_CASE | HELLO_WORLD |
| FLAT_CASE            | helloworld  |
