module.exports = {
  semi: false,
  singleQuote: true,
  quoteProps: "as-needed",
  trailingComma: "es5",
}