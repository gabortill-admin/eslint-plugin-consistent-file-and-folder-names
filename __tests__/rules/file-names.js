const rule = require('../../lib/rules/file-names')
const RuleTester = require('eslint').RuleTester

const ruleTester = new RuleTester()

ruleTester.run('filename-naming-convention', rule, {
  valid: [
    {
      code: '',
      filename: 'root-folder/flatcase.ts',
      options: [{ 'root-folder': 'FLAT_CASE' }],
    },
    {
      code: '',
      filename: 'root-folder/camelCase.ts',
      options: [{ 'root-folder': 'CAMEL_CASE' }],
    },
    {
      code: '',
      filename: 'root-folder/PascalCase.ts',
      options: [{ 'root-folder': 'PASCAL_CASE' }],
    },
    {
      code: '',
      filename: 'root-folder/snake_case.ts',
      options: [{ 'root-folder': 'SNAKE_CASE' }],
    },
    {
      code: '',
      filename: 'root-folder/kebab-case.ts',
      options: [{ 'root-folder': 'KEBAB_CASE' }],
    },
    {
      code: '',
      filename: 'root-folder/SCREAMING_SNAKE_CASE.ts',
      options: [{ 'root-folder': 'SCREAMING_SNAKE_CASE' }],
    },
    {
      code: '',
      filename: 'root-folder/nested-folder/flatcase.ts',
      options: [{ 'root-folder/': 'FLAT_CASE' }],
    },
    {
      code: '',
      filename: 'root-folder/nested-folder/flatcase.ts',
      options: [{ 'root-folder/nested-folder': 'FLAT_CASE' }],
    },
    {
      code: '',
      filename: 'root-folder/nested-folder/another-nested-folder/flatcase.ts',
      options: [
        { 'root-folder/nested-folder/another-nested-folder/': 'FLAT_CASE' },
      ],
    },
    {
      code: '',
      filename: 'root-folder/nested-folder/another-nested-folder/flatcase.ts',
      options: [{ 'nested-folder': 'FLAT_CASE' }],
    },
    {
      code: '',
      filename: 'root-folder/nested-folder/another-nested-folder/flatcase.ts',
      options: [{ 'another-nested-folder/': 'FLAT_CASE' }],
    },
    {
      code: '',
      filename: 'root-folder/nested-folder/another-nested-folder/flatcase.ts',
      options: [{ 'root-folder': 'FLAT_CASE' }],
    },
    {
      code: '',
      filename: 'root-folder/nested-folder/another-nested-folder/flatcase.ts',
      options: [{ 'root-folder': 'CAMEL_CASE' }],
    },
  ],

  invalid: [
    {
      code: '',
      filename: 'root-folder/nested-folder/another-nested-folder/flatcase.ts',
      options: [{ 'root-folder': 'blahblahblah' }],
      errors: [
        'The provided naming convention "blahblahblah" is not recognizable',
      ],
    },
    {
      code: '',
      filename:
        'root-folder/nested-folder/another-nested-folder/SCREAMING_SNAKE_CASE.ts',
      options: [{ 'root-folder': 'PASCAL_CASE' }],
      errors: [
        'Filname "SCREAMING_SNAKE_CASE.ts" in path "root-folder/nested-folder/another-nested-folder/SCREAMING_SNAKE_CASE.ts" does not match with the "PASCAL_CASE" naming convention',
      ],
    },
    {
      code: '',
      filename:
        'root-folder/nested-folder/another-nested-folder/SCREAMING_SNAKE_CASE.ts',
      options: [{ 'root-folder': 'FLAT_CASE' }],
      errors: [
        'Filname "SCREAMING_SNAKE_CASE.ts" in path "root-folder/nested-folder/another-nested-folder/SCREAMING_SNAKE_CASE.ts" does not match with the "FLAT_CASE" naming convention',
      ],
    },
    {
      code: '',
      filename:
        'root-folder/nested-folder/another-nested-folder/SCREAMING_SNAKE_CASE.ts',
      options: [{ 'root-folder': 'CAMEL_CASE' }],
      errors: [
        'Filname "SCREAMING_SNAKE_CASE.ts" in path "root-folder/nested-folder/another-nested-folder/SCREAMING_SNAKE_CASE.ts" does not match with the "CAMEL_CASE" naming convention',
      ],
    },
    {
      code: '',
      filename:
        'root-folder/nested-folder/another-nested-folder/SCREAMING_SNAKE_CASE.ts',
      options: [{ 'root-folder': 'SNAKE_CASE' }],
      errors: [
        'Filname "SCREAMING_SNAKE_CASE.ts" in path "root-folder/nested-folder/another-nested-folder/SCREAMING_SNAKE_CASE.ts" does not match with the "SNAKE_CASE" naming convention',
      ],
    },
    {
      code: '',
      filename:
        'root-folder/nested-folder/another-nested-folder/SCREAMING_SNAKE_CASE.ts',
      options: [{ 'root-folder': 'KEBAB_CASE' }],
      errors: [
        'Filname "SCREAMING_SNAKE_CASE.ts" in path "root-folder/nested-folder/another-nested-folder/SCREAMING_SNAKE_CASE.ts" does not match with the "KEBAB_CASE" naming convention',
      ],
    },
    {
      code: '',
      filename: 'root-folder/nested-folder/another-nested-folder/flatcase.ts',
      options: [{ 'root-folder': 'SCREAMING_SNAKE_CASE' }],
      errors: [
        'Filname "flatcase.ts" in path "root-folder/nested-folder/another-nested-folder/flatcase.ts" does not match with the "SCREAMING_SNAKE_CASE" naming convention',
      ],
    },
  ],
})
